<!--
.. title: References
.. slug: references
.. date: 2017-12-31 21:27:15 UTC-05:00
.. tags: machine learning, python
.. category: ref
.. link: 
.. description: 
.. type: text
-->

# Books

- [Hands-On Machine Learning with Scikit-Learn and TensorFlow](http://shop.oreilly.com/product/0636920052289.do)
    - [A series of Jupyter notebooks](https://github.com/ageron/handson-ml)
- [Deep Learning](http://www.deeplearningbook.org)

# Bayesian Optimization
- [Bayesian Optimization for Machine Learning and Science](https://www.youtube.com/watch?v=a79klpzaPgY)
- [A Python implementation of global optimization with gaussian processes](https://github.com/fmfn/BayesianOptimization)
- [A Tutorial on Bayesian Optimization for Machine Learning](https://www.iro.umontreal.ca/~bengioy/cifar/NCAP2014-summerschool/slides/Ryan_adams_140814_bayesopt_ncap.pdf)

# Control systems

## Self driving cars
- [Self driving cars, MIT](https://selfdrivingcars.mit.edu/)
- [How to simulate a self-driving car](https://www.youtube.com/watch?v=EaY5QiZwSP4)

## Reinforcement learning
- [Implementation of Reinforcement Learning Algorithms. Python, OpenAI Gym and Tensorflow](https://github.com/dennybritz/reinforcement-learning)
- [Learning Reinforcement Learning (with Code, Exercises and Solutions)](http://www.wildml.com/2016/10/learning-reinforcement-learning/)
- [An introduction series to Reinforcement Learning (RL) with comprehensive step-by-step tutorials](https://github.com/vmayoral/basic_reinforcement_learning)
- [Differentiable Inference and Generative Models](https://www.cs.toronto.edu/~duvenaud/courses/csc2541/)
- [CLSquare](https://github.com/mll-freiburg/clsquare)
- [PIQLE](http://piqle.sourceforge.net/)
- [libgrl](https://code.google.com/archive/p/libpgrl/)
- [RL Toolbox](http://www.igi.tugraz.at/lehre/MLB/WS06/ToolboxTutorial.pdf)
- [RLPy](http://acl.mit.edu/RLPy/)

## Probability inference for learning control
- [PILCO: A Model-Based and Data-Efficient Approach to Policy Search](http://mlg.eng.cam.ac.uk/pub/pdf/DeiRas11.pdf)
- [PILCO slides](https://www.cs.toronto.edu/~duvenaud/courses/csc2541/slides/pilco.pdf)
- [PILCO policy search framework 0.9](http://mloss.org/software/view/508/)
- [PILCO MATLAB](https://github.com/ICL-SML/pilco-matlab)

# Courses

- [Making Neural Nets Uncool Again](http://www.fast.ai)
- [Machine learning courses by Andrew Ng](http://www.andrewng.org/courses/)
- [Deep Learning](https://www.coursera.org/specializations/deep-learning)
- [Introduction to Neural Networks and Machine Learning](http://www.cs.toronto.edu/~tijmen/csc321/)
- [Neural Networks for Machine Learning by Geoffrey Hinton](https://www.coursera.org/learn/neural-networks)
- [Convolutional Neural Networks for Visual Recognition](http://cs231n.stanford.edu)

# Finance

- [Profiting from Python & Machine Learning in the Financial Markets](https://hackernoon.com/unsupervised-machine-learning-for-fun-profit-with-basket-clusters-17a1161e7aa1)
- [Machine Learning for Finance](https://gist.github.com/yhilpisch/648565d3d5d70663b7dc418db1b81676)

# Software

- [Theano](http://deeplearning.net/software/theano/)
- [Scikit-Learn](http://scikit-learn.org/stable/)
- [TensorFlow](https://www.tensorflow.org/)
- [Keras](https://keras.io/)
- [Caffe](http://caffe.berkeleyvision.org/)
- [CNTK](https://github.com/Microsoft/cntk)
- [openai/gym](https://github.com/openai/gym)
- [NumFOCUS](https://www.numfocus.org/sponsored-projects/)

# Tutorials

- [Learning Algorithms – or blend of computation and statistics](http://www.cis.upenn.edu/~danielkh/learn.html)

# Video lectures

- [Neural Networks by Hugo Larochelle](https://www.youtube.com/playlist?list=PL6Xpj9I5qXYEcOhn7TqghAJ6NAPrNmUBH)